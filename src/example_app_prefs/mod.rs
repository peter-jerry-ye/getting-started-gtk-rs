mod imp;

use crate::example_app_window::ExampleAppWindow;
use glib::Object;
use gtk::glib;
use gtk::prelude::*;

glib::wrapper! {
    pub struct ExampleAppPrefs(ObjectSubclass<imp::ExampleAppPrefs>)
        @extends gtk::Dialog, gtk::Window, gtk::Widget;
}

impl ExampleAppPrefs {
    pub fn new(win: &ExampleAppWindow) -> Self {
        Object::new(&[
            ("transient-for", win),
            ("use-header-bar", &1.to_value()),
        ])
        .expect("Failed to create `ExampleAppPrefs`.")
    }
}

impl Default for ExampleAppPrefs {
    fn default() -> Self {
        Object::new(&[]).expect("Failed to create `ExampleAppPrefs`.")
    }
}
