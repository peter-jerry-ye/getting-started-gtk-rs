use gio::Settings;
use glib::subclass::InitializingObject;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::CompositeTemplate;
use gtk::Widget;
use std::cell::RefCell;

// Object holding the state
#[derive(CompositeTemplate, Default)]
#[template(file = "pref.ui")]
pub struct ExampleAppPrefs {
    #[template_child]
    pub font: TemplateChild<Widget>,
    #[template_child]
    pub transition: TemplateChild<Widget>,
    pub settings: RefCell<Option<Settings>>,
}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for ExampleAppPrefs {
    const NAME: &'static str = "ExampleAppPrefs";
    type Type = super::ExampleAppPrefs;
    type ParentType = gtk::Dialog;
    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for ExampleAppPrefs {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        *self.settings.borrow_mut() = Some(Settings::new("org.gtk.exampleapp"));
        self.settings
            .borrow()
            .as_ref()
            .unwrap()
            .bind("font", &self.font.get(), "font")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
        self.settings
            .borrow()
            .as_ref()
            .unwrap()
            .bind("transition", &self.transition.get(), "active-id")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
    }
}

impl WidgetImpl for ExampleAppPrefs {}

impl WindowImpl for ExampleAppPrefs {}

impl DialogImpl for ExampleAppPrefs {}
