mod example_app;
mod example_app_prefs;
mod example_app_window;
use example_app::ExampleApp;
use gtk::prelude::*;

fn main() {
    let app = ExampleApp::new();

    app.run_with_args(&std::env::args().collect::<Vec<_>>());
}
