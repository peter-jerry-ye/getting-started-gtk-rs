use crate::example_app_prefs::ExampleAppPrefs;
use crate::example_app_window::ExampleAppWindow;
use gio::File;
use gio::SimpleAction;
use glib::clone;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;

// Object holding the state
#[derive(Default)]
pub struct ExampleApp;

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for ExampleApp {
    const NAME: &'static str = "ExampleApp";
    type Type = super::ExampleApp;
    type ParentType = gtk::Application;
}

// Trait shared by all GObjects
impl ObjectImpl for ExampleApp {}

// Override virtual methods
impl ApplicationImpl for ExampleApp {
    fn startup(&self, application: &Self::Type) {
        let action_preferences = SimpleAction::new("preferences", None);
        let action_quit = SimpleAction::new("quit", None);

        action_quit.connect_activate(clone!(@weak application =>
            move |_, _| {
                application.quit();
            }
        ));
        action_preferences.connect_activate(clone!(@weak application =>
            move |_, _| {
                let win = application.active_window().unwrap();
                let prefs = ExampleAppPrefs::new(win.downcast_ref::<ExampleAppWindow>().unwrap());
                prefs.present();
            }
        ));

        let quit_accels = ["<Ctrl>Q"];

        self.parent_startup(application);

        application.add_action(&action_preferences);
        application.add_action(&action_quit);
        application.set_accels_for_action("app.quit", &quit_accels);
    }

    fn activate(&self, application: &Self::Type) {
        self.parent_activate(application);
        let window = ExampleAppWindow::new(application);

        window.present();
    }

    fn open(&self, application: &Self::Type, files: &[File], hint: &str) {
        self.parent_open(application, files, hint);
        let windows = application.windows();

        if let Some(win) = windows.get(0) {
            for f in files {
                win.downcast_ref::<ExampleAppWindow>().unwrap().open(f);
            }
            win.present();
        } else {
            let win = ExampleAppWindow::new(application);

            for f in files {
                win.open(f);
            }
            win.present();
        }
    }
}

impl GtkApplicationImpl for ExampleApp {}
