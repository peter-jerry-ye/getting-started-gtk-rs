mod imp;
use gio::ApplicationFlags;
use glib::Object;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;

glib::wrapper! {
    pub struct ExampleApp(ObjectSubclass<imp::ExampleApp>)
        @extends gtk::Application, gio::Application, gio::ActionMap, gtk::Widget;
}

impl ExampleApp {
    pub fn new() -> Self {
        Object::new(&[
            ("application-id", &"org.gtk.exampleapp"),
            ("flags", &ApplicationFlags::HANDLES_OPEN.to_value()),
        ])
        .expect("Failed to create `ExampleApp`.")
    }
}

impl Default for ExampleApp {
    fn default() -> Self {
        Self::new()
    }
}
