use super::ui;
use gio::{MenuModel, PropertyAction, Settings};
use glib::clone;
use glib::subclass::InitializingObject;
use gtk::gio;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{
    Builder, CompositeTemplate, ListBox, MenuButton, Revealer, ScrolledWindow, SearchBar,
    SearchEntry, Stack, Label, TextView, Widget,
};
use std::cell::RefCell;
use ui::MENU_UI;

// Object holding the state
#[derive(CompositeTemplate, Default)]
#[template(file = "window.ui")]
pub struct ExampleAppWindow {
    #[template_child]
    pub stack: TemplateChild<Stack>,
    #[template_child]
    pub gears: TemplateChild<MenuButton>,
    #[template_child]
    pub search: TemplateChild<Widget>,
    #[template_child]
    pub searchbar: TemplateChild<SearchBar>,
    #[template_child]
    pub searchentry: TemplateChild<SearchEntry>,
    #[template_child]
    pub sidebar: TemplateChild<Revealer>,
    #[template_child]
    pub words: TemplateChild<ListBox>,
    #[template_child]
    pub lines: TemplateChild<Label>,
    pub settings: RefCell<Option<Settings>>,
}

impl ExampleAppWindow {}

// The central trait for subclassing a GObject
#[glib::object_subclass]
impl ObjectSubclass for ExampleAppWindow {
    const NAME: &'static str = "ExampleAppWindow";
    type Type = super::ExampleAppWindow;
    type ParentType = gtk::ApplicationWindow;
    fn class_init(klass: &mut Self::Class) {
        Self::bind_template(klass);
    }

    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for ExampleAppWindow {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        let builder = Builder::from_string(MENU_UI);
        let menu = builder.object::<MenuModel>("menu");

        self.gears.get().set_menu_model(menu.as_ref());

        *self.settings.borrow_mut() = Some(Settings::new("org.gtk.exampleapp"));
        let settings = self.settings.borrow();
        settings
            .as_ref()
            .unwrap()
            .bind("transition", &self.stack.get(), "transition-type")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
        settings
            .as_ref()
            .unwrap()
            .bind("show-words", &self.sidebar.get(), "reveal-child")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();

        self.search
            .get()
            .bind_property("active", &self.searchbar.get(), "search-mode-enabled")
            .flags(glib::BindingFlags::BIDIRECTIONAL)
            .build();

        self.sidebar.connect_reveal_child_notify(clone!(@weak obj =>
            move |_| {
                obj.update_words();
                obj.update_lines();
            }
        ));

        let action = settings.as_ref().unwrap().create_action("show-words");
        obj.add_action(&action);

        let action = PropertyAction::new("show-lines", &self.lines.get(), "visible");
        obj.add_action(&action);

        let stack = self.stack.get();
        let searchbar = self.searchbar.get();
        let searchentry = self.searchentry.get();
        stack.connect_visible_child_notify(clone!(@weak stack, @weak searchbar, @weak obj =>
            move |_| {
                if !stack.in_destruction() {
                    searchbar.set_search_mode(false);
                    obj.update_words();
                    obj.update_lines();
                }
            }
        ));

        searchentry.connect_search_changed(clone!(@weak stack =>
            move |entry| {
                let text = entry.text();
                if !text.is_empty() {
                    let tab = stack.visible_child().unwrap().downcast::<ScrolledWindow>().unwrap();
                    let view = tab.child().unwrap().downcast::<TextView>().unwrap();
                    let buffer = view.buffer();

                    let start = buffer.start_iter();
                    if let Some((mut match_start, match_end)) = start.forward_search(text.as_str(), gtk::TextSearchFlags::CASE_INSENSITIVE, None) {
                        buffer.select_range(&match_start, &match_end);
                        view.scroll_to_iter(&mut match_start, 0.0, false, 0.0, 0.0);
                    }
                }
            }
        ));
    }
}

impl WidgetImpl for ExampleAppWindow {}

impl WindowImpl for ExampleAppWindow {}

impl ApplicationWindowImpl for ExampleAppWindow {}
