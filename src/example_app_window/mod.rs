mod imp;
mod ui;

use crate::example_app::ExampleApp;
use glib::clone;
use glib::Object;
use gtk::gio;
use gtk::gio::File;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{Button, ScrolledWindow, Label, TextTag, TextView};
use std::collections::BTreeSet;

glib::wrapper! {
    pub struct ExampleAppWindow(ObjectSubclass<imp::ExampleAppWindow>)
        @extends gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionMap;
}

impl ExampleAppWindow {
    pub fn new(app: &ExampleApp) -> Self {
        Object::new(&[("application", app)]).expect("Failed to create `ExampleAppWindow`.")
    }

    pub fn open(&self, file: &File) {
        let basename = file.basename().unwrap();
        let scrolled = ScrolledWindow::builder()
            .hexpand(true)
            .vexpand(true)
            .build();
        let view = TextView::builder()
            .cursor_visible(false)
            .editable(false)
            .build();
        scrolled.set_child(Some(&view));

        let stack = imp::ExampleAppWindow::from_instance(self).stack.get();
        stack.add_titled(&scrolled, basename.to_str(), basename.to_str().unwrap());

        let buffer = view.buffer();

        if let Ok((content, _)) = file.load_contents::<gio::Cancellable>(None) {
            buffer.set_text(&std::str::from_utf8(&content).unwrap());
        }

        let tag = TextTag::new(None);
        imp::ExampleAppWindow::from_instance(self)
            .settings
            .borrow()
            .as_ref()
            .unwrap()
            .bind("font", &tag, "font")
            .flags(gio::SettingsBindFlags::DEFAULT)
            .build();
        buffer.tag_table().add(&tag);
        buffer.apply_tag(&tag, &buffer.start_iter(), &buffer.end_iter());

        imp::ExampleAppWindow::from_instance(self)
            .search
            .set_sensitive(true);

        self.update_words();
        self.update_lines();
    }

    fn find_word(&self, button: &Button) {
        let words = button.label().unwrap_or(glib::GString::from(""));
        imp::ExampleAppWindow::from_instance(self)
            .searchentry
            .get()
            .set_text(words.as_str());
    }
    pub fn update_words(&self) {
        let win = imp::ExampleAppWindow::from_instance(self);
        if let Some(tab) = win.stack.get().visible_child() {
            let view = tab
                .downcast_ref::<ScrolledWindow>()
                .unwrap()
                .child()
                .unwrap()
                .downcast::<TextView>()
                .unwrap();
            let buffer = view.buffer();

            let mut strings = BTreeSet::new();

            let mut start = buffer.start_iter();
            'outer: while !start.is_end() {
                while !start.starts_word() {
                    if !start.forward_char() {
                        break 'outer;
                    }
                }
                let mut end = start.clone();
                if !end.forward_word_end() {
                    break 'outer;
                }
                let word = buffer.text(&start, &end, false);
                strings.insert(word.as_str().to_string());
                start = end;
            }

            while let Some(child) = win.words.get().first_child() {
                win.words.get().remove(&child);
            }

            for word in strings {
                let row = Button::with_label(word.as_str());
                let win_ = self;
                row.connect_clicked(clone!(@weak win_ =>
                    move |button| {
                        win_.find_word(button);
                    }
                ));
                win.words.get().insert(&row, -1);
            }
        }
    }
    pub fn update_lines(&self) {
        if let Some(tab) = imp::ExampleAppWindow::from_instance(self)
            .stack
            .get()
            .visible_child()
        {
            let view = tab
                .downcast_ref::<ScrolledWindow>()
                .unwrap()
                .child()
                .unwrap()
                .downcast::<TextView>()
                .unwrap();
            let buffer = view.buffer();
            let count = buffer.line_count();
            let lines = count.to_string();
            imp::ExampleAppWindow::from_instance(self)
                .lines
                .get()
                .downcast::<Label>()
                .unwrap()
                .set_text(&lines);
        }
    }
}

impl Default for ExampleAppWindow {
    fn default() -> Self {
        Object::new(&[]).expect("Failed to create `ExampleAppWindow`.")
    }
}
