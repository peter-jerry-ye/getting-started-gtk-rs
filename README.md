# 简介

这个是[GTK4.0教程](https://docs.gtk.org/gtk4/getting_started.html)的Rust实现。

为了重复利用`/target`文件夹（单次编译一般在1个G左右），请跳到对应的提交查看。

由于最后部分将会用到教程中的代码，因此许可证必须与教程代码一致，即LGPL 2.0及之后版本。

# 勘误

依赖文件(`Config.toml`)中的`sled`和`chrono`被错误添加。它们与这个项目无关，可以删除。

# Basics 基础

[51fb97ad](../51fb97adefb96ac15693f138e34a192e914a6a9a)

对应[教程Basics部分](https://docs.gtk.org/gtk4/getting_started.html#basics)。

建立了一个200*200的空窗体。

# Hello, World 你好，世界

[8efeca27](../8efeca27cad2cd9755633d70441a07c2d43fdffe)

对应[教程Hello, World部分](https://docs.gtk.org/gtk4/getting_started.html#hello-world)。

建立一个写着"Hello, World"的按钮，按下后会打印出"Hello World"并关闭窗体。

# Packing 布局

[581707d6](../581707d612bce9760dfaaeb3de550c1e34b2d6bb)

对应[教程Packing部分](https://docs.gtk.org/gtk4/getting_started.html#packing)。

建立一个包含三个按钮的窗体并布局。

# Custom Drawing 绘画

[95cfd0d5](../95cfd0d54d01ecc80a2a9e06fdd59871c51e9570)

对应[教程Custom Drawing部分](https://docs.gtk.org/gtk4/getting_started.html#custom-drawing)。

建立一个带绘画区域的窗体。按住左键绘制，按右键或改变窗体大小消除图案。

根据[gtk-rs官方教程](https://gtk-rs.org/gtk4-rs/stable/latest/book/gobject_memory_management.html)，静态指针改为`Rc<RefCell<T>>`。

# Building user interfaces 创建交互界面

[f255231](../f255231dd270f300f1304b687923ab5aa5020317)

对应[教程Building user interfaces部分](https://docs.gtk.org/gtk4/getting_started.html#building-user-interfaces)。

通过导入XML定义的界面文件来生成窗体。

由于cargo不具备类似maven或gradle的资源文件管理功能，我将XML文件直接当作字符串放入程序中。

# Building application 创建应用

对应[教程Building application部分](https://docs.gtk.org/gtk4/getting_started.html#building-applications)。

创建一个应用。

鉴于不想折腾桌面文件、图标、GSettings设置、资源文件等，内容将会有较大程度的精简。想要了解如何使用Meson打包系统，建议参考[官方模板](https://gitlab.gnome.org/bilelmoussaoui/gtk-rust-template)。

## A trivial application 一个简单的应用

[e0a6d74](../e0a6d745ee6907d4200e1dfc06d26000cea8220c)

对应[教程A trivial application部分](https://docs.gtk.org/gtk4/getting_started.html#a-trivial-application)。

创建一个只有窗体的简单的应用。

省略了桌面文件及图标。

## Populating the window 填充窗体

[c0f9978](../c0f99789aa8f8165661d4570ce6b91fb763adb91)

对应[教程Populating the window部分](https://docs.gtk.org/gtk4/getting_started.html#populating-the-window)。

通过ui文件定义窗体的大小、标题、布局。

如上所述，ui文件直接作为字符串放入代码避免打包问题。

## Opening files 打开文件

[dc67f3d](../dc67f3d7d41b1e24bb23394d031583254c2fe186)

对应[教程Opening files部分](https://docs.gtk.org/gtk4/getting_started.html#opening-files)。

添加查看文件功能，可打开多个文件。参数在启动时传递。

例：`cargo run -- README.md LICENSE .gitignore`。

## A menu 菜单

[5b66db6](../5b66db61e4e9d68f6d3e9df8c4ee7fc3ed4d0bd6)

对应[教程A Menu部分](https://docs.gtk.org/gtk4/getting_started.html#a-menu)。

添加一个包含退出菜单项的菜单，及退出快捷键组合。

## A preference dialog 偏好设置

[7fd5e85](../7fd5e8537d684a5e5adbdde20951170cd0a1b065)

对应[教程A preference dialog部分](https://docs.gtk.org/gtk4/getting_started.html#a-preference-dialog)。

添加一个可设置字体的对话框。

### 设置GSettings文件

运行时需安装`org.gtk.exampleapp.gschema.xml`。

#### 标准设置

```bash
$ sudo install -D org.gtk.example.gschema.xml /usr/share/glib-2.0/schemas/
$ sudo glib-compile-schemas /usr/share/glib-2.0/schemas/
```

这个命令将会把`org.gtk.example.gschema.xml`文件复制到惯例目录中并编译，之后只需正常使用`cargo run`即可运行。

#### 非标准安装

```bash
$ glib-compile-schemas .
$ GSETTINGS_SCHEMA_DIR=. cargo run --README.md
```

以上是不移动`org.gtk.example.gschema.xml`的例子，直接在当前文件中编译，无需root权限，但是运行时需指定放置GSettings Schema的文件夹。

## Adding a search bar 添加搜索栏

[f98f046](../f98f04620c7d701bf528dac062d3910a3bd5be92)

对应[教程Adding a search bar部分](https://docs.gtk.org/gtk4/getting_started.html#adding-a-search-bar)。

添加一个搜索栏。

我没有在gtk-rs中找到对应`template_bind_callback`的方法，因此没有采用这种方式，而是手动添加信号。

## Adding a side bar 添加侧边栏

[2206285](../2206285a45b6932341d32b1d8676fd01e79b9a7a)

对应[教程Adding a side bar部分](https://docs.gtk.org/gtk4/getting_started.html#adding-a-side-bar)。

添加一个侧边栏。统计所有单词后生成每个单词对应的按钮，用于跳转到该位置。

鉴于Rust有自己的集合，我没有采用GTK的集合。

## Properties 属性

[2045e10](../2045e103bee73203cabb88d5f34ca5fbd361625c)

对应[教程Properties部分](https://docs.gtk.org/gtk4/getting_started.html#properties)。

利用属性控制行数的显示与否。

# 总结

教程到这里就结束了。希望这个仓库对你有帮助。